import org.scalatest.{ FunSpec, Matchers }


class CombinatoricsSpec extends FunSpec with Matchers {
  import Combinatorics._

  describe("Combinatorics") {
    describe("combinations") {
      it("should find all combinations of length 1") {
        val input = List(1, 2, 3)
        val expected = List(List(1), List(2), List(3))
        val result = combinations(input, 1)
        result should equal (expected)
      }

      it("should find all combinations of length 2") {
        val input = List(1, 2, 3)
        val expected = List(
          List(1, 1), List(1, 2), List(1, 3),
          List(2, 1), List(2, 2), List(2, 3),
          List(3, 1), List(3, 2), List(3, 3))
        val result = combinations(input, 2)
        result should equal (expected)
      }

      it("should find all combinations of length 3") {
        val input = List(1, 2, 3)
        val expected = List(
          List(1, 1, 1), List(1, 1, 2), List(1, 1, 3),
          List(1, 2, 1), List(1, 2, 2), List(1, 2, 3),
          List(1, 3, 1), List(1, 3, 2), List(1, 3, 3),
          List(2, 1, 1), List(2, 1, 2), List(2, 1, 3),
          List(2, 2, 1), List(2, 2, 2), List(2, 2, 3),
          List(2, 3, 1), List(2, 3, 2), List(2, 3, 3),
          List(3, 1, 1), List(3, 1, 2), List(3, 1, 3),
          List(3, 2, 1), List(3, 2, 2), List(3, 2, 3),
          List(3, 3, 1), List(3, 3, 2), List(3, 3, 3))
        val result = combinations(input, 3)
        result should equal (expected)
      }

      it("should find 4 ^ 6 combinations of length 6 with 4 elements") {
        val input = List(1, 2, 3, 4)
        val result = combinations(input, 6)
        result.size should equal (math.pow(4, 6))
      }
    }

    describe("collectCombinations") {
      it("should collect and count three of the same combinations") {
        val input = List(List(1, 1), List(1, 1), List(1, 1))
        val expected = List(CombinationCount(3, List(1, 1)))
        val result = collectCombinations(input)
        result should equal (expected)
      }

      it("should collect and count four of the same combinations") {
        val input = List(List(1, 1), List(1, 1), List(1, 1), List(1, 1))
        val expected = List(CombinationCount(4, List(1, 1)))
        val result = collectCombinations(input)
        result should equal (expected)
      }

      it("should collect and count multiple combinations") {
        val input = List(
          List(1, 1), List(1, 1), List(1, 1), List(1, 1),
          List(1, 2), List(1, 2), List(1, 2))
        val expected = List(
          CombinationCount(4, List(1, 1)),
          CombinationCount(3, List(1, 2)))
        val result = collectCombinations(input)
        result should equal (expected)
      }
    }

    val outcomeDefinition = (combination: Seq[Int]) => {
      if (combination(0) <= combination(1)) {
        (combination(0), combination(1))
      } else {
        (combination(1), combination(0))
      }
    }

    describe("collectOutcomes") {
      it("should collect one combination to an outcome with the outcome definition") {
        val input = List(CombinationCount[Int](1, List(1, 1)))
        val expected = Vector(
          OutcomeCount[Int, (Int, Int)](1, (1, 1), List(List(1, 1))))
        val result = collectOutcomes(input, outcomeDefinition)
        result should equal (expected)
      }

      it("""should collect two combinations with the same outcome definition
            and add all combinations to the outcome count""") {
        val input = List(
          CombinationCount[Int](1, List(1, 2)),
          CombinationCount[Int](1, List(2, 1)))
        val expected = Vector(
          OutcomeCount[Int, (Int, Int)](
            2, (1, 2), List(List(1, 2), List(2, 1))))
        val result = collectOutcomes(input, outcomeDefinition)
        result should equal (expected)
      }

      it("""should collect two combinations with different outcome definitions
            into two outcome counts including the combinations""") {
        val input = List(
          CombinationCount[Int](1, List(1, 1)),
          CombinationCount[Int](1, List(2, 1)))
        val expected = Vector(
          OutcomeCount[Int, (Int, Int)](1, (1, 1), List(List(1, 1))),
          OutcomeCount[Int, (Int, Int)](1, (1, 2), List(List(2, 1))))
        val result = collectOutcomes(input, outcomeDefinition)
        result should equal (expected)
      }

      it("""should collect multiple combinations with different outcome definitions
            into different outcome counts including the combinations. Additionally
            it should take the combination count into account.""") {
        val input = List(
          CombinationCount[Int](1, List(1, 1)),
          CombinationCount[Int](4, List(1, 2)),
          CombinationCount[Int](3, List(1, 3)),
          CombinationCount[Int](5, List(3, 1)),
          CombinationCount[Int](2, List(2, 1)))
        val expected = Vector(
          OutcomeCount[Int, (Int, Int)](1, (1, 1), List(List(1, 1))),
          OutcomeCount[Int, (Int, Int)](6, (1, 2), List(List(1, 2), List(2, 1))),
          OutcomeCount[Int, (Int, Int)](8, (1, 3), List(List(1, 3), List(3, 1)))).
          sortBy(c => c.count)
        val result = collectOutcomes(input, outcomeDefinition).sortBy(c => c.count)
        result should equal (expected)
      }
    }
  }
}
