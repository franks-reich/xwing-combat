object Combinatorics {
  type Combination[A] = Seq[A]
  type Combinations[A] = Seq[Combination[A]]

  case class CombinationCount[A](
    val count: Int,
    val combination: Combination[A])

  case class OutcomeCount[A, B](
    val count: Int,
    val outcome: B,
    val combinations: Iterable[Combination[A]])

  type CombinationCounts[A] = Seq[CombinationCount[A]]

  def collectCombinations[A](
    input: Iterable[Combination[A]]): CombinationCounts[A] =
      input.groupBy(x => x).
        map(y => CombinationCount(y._2.size, y._2.head)).toVector

  def collectOutcomes[A, B](
    input: CombinationCounts[A],
    outcomeDefinition: Combination[A] => B): Seq[OutcomeCount[A, B]] =
      input.groupBy(c => outcomeDefinition(c.combination)).
        map(m => OutcomeCount(
          m._2.map(c => c.count).sum,
          m._1,
          m._2.map(c => c.combination))).toVector

  def combinations[A](
    values: Iterable[A],
    length: Int,
    start: Iterable[Combination[A]] = Vector()): Iterable[Combination[A]] = {

    val expand = (start: Iterable[Combination[A]], values: Iterable[A]) =>
      start match {
        case Vector() => values.map(v => Vector(v))
        case _ => start.flatMap(list => values.
          map(value => list :+ value))
      }

    length match {
      case 1 => expand(start, values)
      case _ => combinations(values, length - 1, expand(start, values))
    }
  }
}
