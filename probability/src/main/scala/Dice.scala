abstract class DieSide(val name: String, val order: Int)


object AttackDie {
  import Combinatorics._

  sealed abstract class Side(name: String, order: Int) extends DieSide(name, order)
  case object Blank extends Side("Blank", 0)
  case object Focus extends Side("Focus", 1)
  case object Hit extends Side("Hit", 2)
  case object Crit extends Side("Crit", 3)

  val sides = Vector(Blank, Blank, Focus, Focus, Hit, Hit, Hit, Crit)

  case class Outcome(val hits: Int, val crits: Int)

  def allCombinations(count: Int): Combinations[Side with Product with Serializable] = combinations(AttackDie.sides, count).
    map(v => v.sortBy(y => y.order)).toVector

  def collectedCombinations(count: Int) = 
    collectCombinations(allCombinations(count))
  
  def outcome(combination: Combination[Side with Product with Serializable]) =
    Outcome(
      combination.count(die => die == Hit),
      combination.count(die => die == Crit))
  
  def collectedOutcomes(count: Int) =
    collectOutcomes(collectedCombinations(count), outcome)
  
  def totalHits(combination: Combination[Side with Product with Serializable]) =
    combination.count(die => die == Hit) + combination.count(die => die == Crit)

  def collectedHits(count: Int) =
    collectOutcomes(collectedCombinations(count), totalHits).sortBy(o => o.outcome)
}


object DefenseDie {
  sealed abstract class Side(name: String, order: Int) extends DieSide(name, order)
  case object Blank extends Side("Blank", 0)
  case object Focus extends Side("Focus", 1)
  case object Evade extends Side("Evade", 2)
  val sides = Vector(Blank, Blank, Blank, Focus, Focus, Evade, Evade, Evade)
}
