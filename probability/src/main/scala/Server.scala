import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives
import akka.stream.ActorMaterializer
import scala.io.StdIn


object Server extends Directives with JsonSupport {
  def main(args: Array[String]) {
    implicit val system = ActorSystem("xwing-probability")
    implicit val materializer = ActorMaterializer()
    implicit val executionContext = system.dispatcher

    val route =
      pathPrefix("api") {
        pathPrefix("xwing") {
          pathPrefix("attack_dice_combinations") {
            path("collected_outcomes" / IntNumber) { count =>
              get { 
                if (0 < count && count < 7) {
                  complete(AttackDie.collectedOutcomes(count))
                } else {
                  complete(StatusCodes.BadRequest);
                }
              }
            } ~
            path("collected_combinations" / IntNumber) { count =>
              get {
                if (0 < count && count < 7) {
                  complete(AttackDie.collectedCombinations(count))
                } else {
                  complete(StatusCodes.BadRequest);
                }
              }
            } ~
            path("collected_total_hits" / IntNumber) { count =>
              get {
                if (0 < count && count < 7) {
                  complete(AttackDie.collectedHits(count))
                } else {
                  complete(StatusCodes.BadRequest);
                }
              }
            }
          }
        }
      }

    val bindingFuture = Http().bindAndHandle(route, "localhost", 9090)
    println(s"Server online at http://localhost:9090/\nPress RETURN to stop...")
    StdIn.readLine()
    bindingFuture
      .flatMap(_.unbind())
      .onComplete(_ => system.terminate())
  }
}
