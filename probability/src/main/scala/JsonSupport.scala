import scala.language.implicitConversions
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json._


trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  import Combinatorics._

  type AttackDieProduct = Product with Serializable with AttackDie.Side
  type RootJsonAttackDieProduct = RootJsonFormat[AttackDieProduct]
  type AttackDieCombinationCount = CombinationCount[AttackDieProduct]
  type RootJsonAttackDieCombinationCount = RootJsonFormat[AttackDieCombinationCount]
  type AttackDieOutcomeCount = OutcomeCount[AttackDieProduct, AttackDie.Outcome]
  type RootJsonAttackDieOutcomeCount = RootJsonFormat[AttackDieOutcomeCount]
  type AttackDieHitCount = OutcomeCount[AttackDieProduct, Int]
  type RootJsonAttackDieHitCount = RootJsonFormat[AttackDieHitCount]

  implicit val printer = PrettyPrinter

  implicit def dieSideToJsObject(side: DieSide) = JsObject(
    "name" -> side.name.toJson
  )

  implicit object AttackDieSideFormat extends RootJsonAttackDieProduct {
    def write(side: AttackDieProduct): JsObject = side

    def read(value: JsValue) = {
      AttackDie.Blank
    }
  }

  implicit object AttackDieCombinationCountFormat extends RootJsonAttackDieCombinationCount {
    def write(combinationCount: AttackDieCombinationCount) = {
      JsObject(
        "count" -> combinationCount.count.toJson,
        "combination" -> combinationCount.combination.toJson
      )
    }

    def read(value: JsValue) = {
      CombinationCount[AttackDieProduct](0, List())
    }
  }

  implicit val attackDieOutcomeFormat = jsonFormat2(AttackDie.Outcome)

  implicit object AttackDieOutcomeCountFormat extends RootJsonAttackDieOutcomeCount {
    def write(outcomeCount: AttackDieOutcomeCount) = {
      JsObject(
        "count" -> outcomeCount.count.toJson,
        "outcome" -> outcomeCount.outcome.toJson,
        "combinations" -> outcomeCount.combinations.toJson
      )
    }

    def read(value: JsValue) = {
      OutcomeCount[AttackDieProduct, AttackDie.Outcome](
        0, AttackDie.Outcome(0, 0), List())
    }
  }

  implicit object AttackDieHitCountFormat extends RootJsonAttackDieHitCount {
    def write(outcomeCount: AttackDieHitCount) = {
      JsObject(
        "count" -> outcomeCount.count.toJson,
        "outcome" -> outcomeCount.outcome.toJson,
        "combinations" -> outcomeCount.combinations.toJson
      )
    }

    def read(value: JsValue) = {
      OutcomeCount[AttackDieProduct, Int](
        0, 0, List())
    }
  }
}
