import Dependencies._

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "net.franks-reich",
      scalaVersion := "2.12.4",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "probability",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http" % "latest.integration",
      "com.typesafe.akka" %% "akka-stream" % "latest.integration",
      "com.typesafe.akka" %% "akka-http-spray-json" % "latest.integration",
      scalaTest % Test
    )
  )
